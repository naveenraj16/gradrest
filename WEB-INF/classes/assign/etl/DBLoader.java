package assign.etl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import assign.domain.MastersStudent;
import assign.domain.MastersStudents;
import assign.domain.PhdStudent;
import assign.domain.PhdStudents;

public class DBLoader {
	
	String dbURL = "";
	String dbUsername = "";
	String dbPassword = "";
	DataSource ds;
	
	Logger logger = Logger.getLogger(DBLoader.class.getName());
	
	public DBLoader(String dbUrl, String username, String password) {
		this.dbURL = dbUrl;
		this.dbUsername = username;
		this.dbPassword = password;
		
		ds = setupDataSource();
	}
	
	public DataSource setupDataSource() {
		BasicDataSource ds = new BasicDataSource();
	    ds.setUsername(this.dbUsername);
	    ds.setPassword(this.dbPassword);
	    ds.setUrl(this.dbURL);
	    ds.setDriverClassName("com.mysql.jdbc.Driver");
	    return ds;
	}
	
	public void loadData(List<MastersStudent> m, List<PhdStudent> p) throws Exception {
		logger.info("Inside loadData.");
		
		//load masters students
		
		Connection conn = ds.getConnection();
		String insert = "INSERT INTO masters(snum,sname,type,advisor,month,year) VALUES(?,?,?,?,?,?)";
		PreparedStatement stmt = conn.prepareStatement(insert);
		for(int i = 0; i < m.size(); i++){
			MastersStudent s = m.get(i);
			stmt.setInt(1, s.getSnum());
			stmt.setString(2, s.getSname());
			stmt.setString(3, s.getType());
			stmt.setString(4, s.getAdvisor());
			stmt.setInt(5, s.getMonth());
			stmt.setInt(6, s.getYear());
			int affectedRows = stmt.executeUpdate();

	        if (affectedRows == 0) {
	            throw new SQLException("Creating student failed, no rows affected.");
	        }
		}
		
		//load Phd Students
		
		insert = "INSERT INTO phd(snum,sname,advisor,coadvisor,month,year) VALUES(?,?,?,?,?,?)";
		stmt = conn.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
		for(int i = 0; i < p.size(); i++){
			PhdStudent ps = p.get(i);
			stmt.setInt(1, ps.getSnum());
			stmt.setString(2, ps.getSname());
			stmt.setString(3, ps.getAdvisor());
			stmt.setString(4, ps.getCoadvisor());
			stmt.setInt(5, ps.getMonth());
			stmt.setInt(6, ps.getYear());
			int affectedRows = stmt.executeUpdate();

	        if (affectedRows == 0) {
	            throw new SQLException("Creating student failed, no rows affected.");
	        }
		}
		
		 stmt.close();
	     // Close the connection
	     conn.close();
	}
}