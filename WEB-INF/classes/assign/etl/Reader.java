package assign.etl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import assign.domain.MastersStudent;
import assign.domain.PhdStudent;

public class Reader {
	
	String url;
	Logger logger; 

	
	public Reader(String url) {
		this.url = url;
		
		logger = Logger.getLogger("Reader");
	}
	
	/*
	 * get masters students
	 */
	public List<MastersStudent> readMastersData() {
		
		logger.info("Inside readData.");
		
		List<MastersStudent> data = new ArrayList<MastersStudent>();
		
		Document doc;
		
		try {
			
			// need http protocol
			doc = Jsoup.connect(url).get();
	 
	 
			// get all links
			Elements ul = doc.getElementsByTag("ul").get(0).children();
			String month = "";
			String year = "";
			int num=0;
			for(int i = 0; i < ul.size(); i++){
				Element e = ul.get(i); 
				//System.out.println("element####: "+e);
				if(e.nodeName().equals("a"))
					continue;
				if(e.nodeName().equals("h4")){
					String text = e.ownText();
					int index1 = text.indexOf(' ');
					month = text.substring(0, index1).trim();
					int index2 = text.indexOf(' ', index1 + 1);
					year = text.substring(index1 + 1, index2).trim();
					int index3 = text.indexOf('(', index2);
					int index4 = text.indexOf(')');
					num = Integer.parseInt(text.substring(index3+1,index4));
				}
				else{//must be li element
						//  System.out.println("iii= "+i);
						e = ul.get(i);
						String nameText = e.text();
					//System.out.println("NAME TEXT: "+nameText);
						int ind1 = nameText.indexOf('(');
						String name = "";
						String type = "";
						String advisor = "";
						if(ind1 != -1){
							if(e.toString().contains("<h4>")){
System.out.println(e.toString());
								String elementText = e.toString();
								int ind5 = elementText.indexOf("<h4>");
								int ind6 = elementText.indexOf('(');
								if(ind6 >  ind5){
									int ind7 = elementText.indexOf("<a name");
									if(ind7 != -1)
										name = elementText.substring(5,ind7-1);
									else
										name = elementText.substring(5,ind5-1);
								}
								else{
									name = nameText.substring(0,ind1-1).trim();
									int ind2 = nameText.indexOf(',',ind1+1);
									if(ind2 == -1){
										int ind3 = nameText.indexOf(")");
										type = nameText.substring(ind1+1,ind3).trim();
									}
								
									else{
										int ind3 = nameText.indexOf(")");
										type = nameText.substring(ind1+1, ind2).trim();
										advisor = nameText.substring(ind2+1,ind3).trim();
									}
								}
							}
							else{
								name = nameText.substring(0,ind1-1).trim();
								int ind2 = nameText.indexOf(',',ind1+1);
								if(ind2 == -1){
									int ind3 = nameText.indexOf(")");
									type = nameText.substring(ind1+1,ind3).trim();
								}
							
								else{
									int ind3 = nameText.indexOf(")");
									type = nameText.substring(ind1+1, ind2).trim();
									advisor = nameText.substring(ind2+1,ind3).trim();
								}
							}
						}
						else{
							name = nameText.trim();
						}
						
						int y = Integer.parseInt(year);
						int m = 0;
						if(month.equals("May"))
							m = 5;
						else if(month.equals("August"))
							m = 8;
						else
							m = 12;
						MastersStudent ms = new MastersStudent(data.size()+1,name,type,advisor,m,y);
						//System.out.println("PRIMARY KEY: "+(data.size()+1));
						data.add(ms);
						//System.out.println("i: "+i+" name: "+name);
						//System.out.println("element####: "+e);
						
						if(e.toString().contains("<h4>")){
							String text = e.toString();
							ind1 = text.indexOf("<h4>") + 4;
							int ind2 = text.indexOf(" ", ind1);
							month = text.substring(ind1,ind2);
							year = text.substring(ind2+1,ind2+5);
						}
					
				}
			}
	 
		} catch (IOException e) {
			//e.printStackTrace();
			return data;
		}
				
		return data;
	}
	
	
	
	 // get phd students 
	public List<PhdStudent> readPhdData() {

		logger.info("Inside readData.");
		
		List<PhdStudent> data = new ArrayList<PhdStudent>();
		
		Document doc;
		
		try {
			
			// need http protocol
			doc = Jsoup.connect(url).get();
	 
	 
			// get all links
			Elements ol = doc.getElementsByTag("ol").get(0).children();
			String month = "";
			String year = "";
			int num=0;
			for(int i = 0; i < ol.size(); i++){
				Element e = ol.get(i); 
			//System.out.println("element String text: "+e.text());
				if(e.nodeName().equals("h4")){
				  	String text = e.ownText();
					int index1 = text.indexOf(' ');
					month = text.substring(0, index1).trim();
					year = text.substring(index1 + 1, text.length()).trim();
				}
				else{//must be li element
					//  System.out.println("iii= "+i);
					e = ol.get(i);
					String elementText = e.toString();
					int ind1 = elementText.indexOf('"');
					int ind2 = elementText.indexOf('"',ind1+1);
					int id = Integer.parseInt(elementText.substring(ind1+1,ind2));
					String nameText = e.text();
					int ind3 = nameText.indexOf('(');
					String name = nameText.substring(0,ind3).trim();
					String advisor = "";
					String coadvisor = "";
					int ind4 = nameText.indexOf(";",ind3);
					//if they had a co advisor
					if(ind4 != -1){
						advisor = nameText.substring(ind3+1,ind4).trim();
						int ind5 = nameText.indexOf(':');
						int ind6 = nameText.indexOf(')');
						coadvisor = nameText.substring(ind5+2,ind6).trim();
					}
					else{
						int ind6 = nameText.indexOf(')');
						advisor = nameText.substring(ind3+1,ind6).trim();
					}
					
					int y = Integer.parseInt(year);
					int m = 0;
					if(month.equals("May"))
						m = 5;
					else if(month.equals("August"))
						m = 8;
					else
						m = 12;
					
					PhdStudent ps = new PhdStudent(id,name,advisor,coadvisor,m,y);
					//System.out.println("PRIMARY KEY: "+(data.size()+1));
					data.add(ps);
					//System.out.println("i: "+i+" name: "+name);
					//System.out.println("element####: "+e);
					
					if(e.toString().contains("<h4>")){
						String text = e.toString();
						ind1 = text.indexOf("<h4>") + 4;
						ind2 = text.indexOf(" ", ind1);
						month = text.substring(ind1,ind2);
						year = text.substring(ind2+1,ind2+5);
					}
				
				}
			}
	 
		} catch (IOException e) {
			//e.printStackTrace();
			return data;
		}
				
		return data;
	}
}