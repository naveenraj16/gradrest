package assign.etl;

import java.util.List;
import java.util.Map;

import assign.domain.MastersStudent;
import assign.domain.PhdStudent;

public class ETLController {
	
	Reader reader;
	DBLoader loader;
	
	public ETLController() {
		loader = new DBLoader("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	}
	
	public static void main(String[] args) {
		ETLController etlController = new ETLController();
		etlController.performETLActions();
	}

	private void performETLActions() {		
		try {
			
			//masters
			String source = "http://tinman.cs.gsu.edu/~raj/GRAD/ms-graduates.html";
			reader = new Reader(source);
			// Read data
			List<MastersStudent> mastersData = reader.readMastersData();
			
			
			//phd
			source = "http://tinman.cs.gsu.edu/~raj/GRAD/phd-graduates.html";
			reader = new Reader(source);
			// Read data
			List<PhdStudent> phdData = reader.readPhdData();
			
			// Load all data
			loader.loadData(mastersData,phdData);

		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
}
