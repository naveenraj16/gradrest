package assign.etl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.*;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;

import assign.domain.MastersStudent;
import assign.domain.MastersStudents;
import assign.domain.PhdStudent;
import assign.domain.PhdStudents;

public class DBQuery {
	
	String dbURL = "";
	String dbUsername = "";
	String dbPassword = "";
	DataSource ds;
	
	Logger logger = Logger.getLogger(DBLoader.class.getName());
	
	public DBQuery(String dbUrl, String username, String password) {
		this.dbURL = dbUrl;
		this.dbUsername = username;
		this.dbPassword = password;
		
		ds = setupDataSource();
	}
	
	public DataSource setupDataSource() {
		BasicDataSource ds = new BasicDataSource();
	    ds.setUsername(this.dbUsername);
	    ds.setPassword(this.dbPassword);
	    ds.setUrl(this.dbURL);
	    ds.setDriverClassName("com.mysql.jdbc.Driver");
	    return ds;
	}
	
	
	//Get all of the masters students
	public MastersStudents getMastersStudents() throws Exception {
		MastersStudents m = new MastersStudents();
		List<MastersStudent> ms = new ArrayList<MastersStudent>();
    	Connection conn = ds.getConnection();
    	String get = "select * from masters";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		MastersStudent s = new MastersStudent();
    		s.setSnum(rs.getInt(1));
    		s.setSname(rs.getString(2));
        	s.setType(rs.getString(3));
        	s.setAdvisor(rs.getString(4));
        	s.setMonth(rs.getInt(5));
        	s.setYear(rs.getInt(6));
        	ms.add(s);
    	}
    	
    	m.setStudents(ms);
    	
    	stmt.close();
   		conn.close();
		
    	return m;		
	}
	
	//Get all of the masters students
	public MastersStudents getMastersStudentsForYear(int year) throws Exception {
		MastersStudents m = new MastersStudents();
		List<MastersStudent> ms = new ArrayList<MastersStudent>();
    	Connection conn = ds.getConnection();
    	String get = "select snum, sname, type, advisor, month from masters where year=?";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setInt(1,year);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		MastersStudent s = new MastersStudent();
    		s.setSnum(rs.getInt(1));
    		s.setSname(rs.getString(2));
        	s.setType(rs.getString(3));
        	s.setAdvisor(rs.getString(4));
        	s.setMonth(rs.getInt(5));
        	s.setYear(year);
        	ms.add(s);
    	}
    	
    	m.setStudents(ms);
    	
    	stmt.close();
   		conn.close();
		
    	return m;		
	}
	
	//Get all phd students
	public PhdStudents getPhdStudents() throws Exception {
		PhdStudents p = new PhdStudents();
		List<PhdStudent> ps = new ArrayList<PhdStudent>();
    	Connection conn = ds.getConnection();
    	String get = "select * from phd";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		PhdStudent s = new PhdStudent();
    		s.setSnum(rs.getInt(1));
    		s.setSname(rs.getString(2));
        	s.setAdvisor(rs.getString(3));
        	s.setCoadvisor(rs.getString(4));
        	s.setMonth(rs.getInt(5));
        	s.setYear(rs.getInt(6));
        	ps.add(s);
    	}
    	
    	p.setStudents(ps);
    	
    	stmt.close();
   		conn.close();
		
    	return p;		
	}
	
	//Get all phd students
	public PhdStudents getPhdStudentsForYear(int year) throws Exception {
		PhdStudents p = new PhdStudents();
		List<PhdStudent> ps = new ArrayList<PhdStudent>();
    	Connection conn = ds.getConnection();
    	String get = "select snum, sname, advisor, coadvisor, month from phd where year=?";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setInt(1,year);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		PhdStudent s = new PhdStudent();
    		s.setSnum(rs.getInt(1));
    		s.setSname(rs.getString(2));
        	s.setAdvisor(rs.getString(3));
        	s.setCoadvisor(rs.getString(4));
        	s.setMonth(rs.getInt(5));
        	s.setYear(year);
        	ps.add(s);
    	}
    	
    	p.setStudents(ps);
    	
    	stmt.close();
   		conn.close();
		
    	return p;		
	}
	
	public MastersStudents getMastersStudentForAdvisor(String advisor) throws Exception {
		MastersStudents m = new MastersStudents();
		List<MastersStudent> ms = new ArrayList<MastersStudent>();
    	Connection conn = ds.getConnection();
    	String get = "select snum, sname, type, month, year from masters where advisor=? order by year,month desc";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		MastersStudent s = new MastersStudent();
    		s.setSnum(rs.getInt(1));
    		s.setSname(rs.getString(2));
        	s.setType(rs.getString(3));
        	s.setAdvisor(advisor);
        	s.setMonth(rs.getInt(4));
        	s.setYear(rs.getInt(5));
        	ms.add(s);
    	}
    	
    	m.setStudents(ms);
    	
    	stmt.close();
   		conn.close();	
    	return m;		
	}
	
	public MastersStudents getMastersStudentForAdvisorAndYear(String advisor,int year) throws Exception {
		MastersStudents m = new MastersStudents();
		List<MastersStudent> ms = new ArrayList<MastersStudent>();
    	Connection conn = ds.getConnection();
    	String get = "select snum, sname, type, month from masters where advisor=? and year=? order by year,month desc";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	stmt.setInt(2,year);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		MastersStudent s = new MastersStudent();
    		s.setSnum(rs.getInt(1));
    		s.setSname(rs.getString(2));
        	s.setType(rs.getString(3));
        	s.setAdvisor(advisor);
        	s.setMonth(rs.getInt(4));
        	s.setYear(year);
        	ms.add(s);
    	}
    	
    	m.setStudents(ms);
    	
    	stmt.close();
   		conn.close();	
    	return m;		
	}
	
	public List<String> getAdvisorNames() throws Exception {
		List<String> a = new ArrayList<String>();
    	Connection conn = ds.getConnection();
    	String get = " (select distinct advisor aname from masters) union "+
    			     " (select distinct advisor aname from phd) union "+
    				 " (select distinct coadvisor aname from phd) order by aname";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		if(!rs.getString(1).equals(""))
    			a.add(rs.getString(1));
    	}
    	
    	stmt.close();
   		conn.close();	
   		
    	return a;		
	}
	
	public List<Integer> getYears() throws Exception {
		List<Integer> y = new ArrayList<Integer>();
    	Connection conn = ds.getConnection();
    	String get = " (select distinct year from masters) union "+
    			     " (select distinct year from phd) order by year";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		y.add(rs.getInt(1));
    	}
    	
    	stmt.close();
   		conn.close();	
   		
    	return y;		
	}
	
	//Get all phd students for an advisor
	public PhdStudents getPhdStudentsAdvisor(String advisor) throws Exception {
		PhdStudents p = new PhdStudents();
		List<PhdStudent> ps = new ArrayList<PhdStudent>();
    	Connection conn = ds.getConnection();
    	String get = "select snum, sname, advisor, coadvisor, month, year from phd where advisor=? or coadvisor=? order by year, month";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	stmt.setString(2,advisor);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		PhdStudent s = new PhdStudent();
    		s.setSnum(rs.getInt(1));
    		s.setSname(rs.getString(2));
        	s.setAdvisor(rs.getString(3));
        	s.setCoadvisor(rs.getString(4));
        	s.setMonth(rs.getInt(5));
        	s.setYear(rs.getInt(6));
        	ps.add(s);
    	}
    	
    	p.setStudents(ps);
    	
    	stmt.close();
   		conn.close();
		
    	return p;		
	}
	
	
	public PhdStudents getPhdStudentsAdvisorAndYear(String advisor,int year) throws Exception {
		PhdStudents p = new PhdStudents();
		List<PhdStudent> ps = new ArrayList<PhdStudent>();
    	Connection conn = ds.getConnection();
    	String get = "select snum, sname, advisor, coadvisor, month from phd where (advisor=? or coadvisor=?) and year=? order by year,month";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	stmt.setString(2,advisor);
    	stmt.setInt(3,year);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next()){
    		PhdStudent s = new PhdStudent();
    		s.setSnum(rs.getInt(1));
    		s.setSname(rs.getString(2));
        	s.setAdvisor(rs.getString(3));
        	s.setCoadvisor(rs.getString(4));
        	s.setMonth(rs.getInt(5));
        	s.setYear(year);
        	ps.add(s);
    	}
    	
    	p.setStudents(ps);
    	
    	stmt.close();
   		conn.close();
		
    	return p;		
	}
	
	public int getMastersStudentForAdvisorCount(String advisor) throws Exception {
		int count = 0;
    	Connection conn = ds.getConnection();
    	String get = "select count(*) from masters where advisor=?";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	ResultSet rs = stmt.executeQuery();
    	if(rs.next()){
    		count = rs.getInt(1);
    	}
    	
    	stmt.close();
   		conn.close();	
   		
    	return count;		
	}
	
	public int getMastersStudentForAdvisorAndYearCount(String advisor,int year) throws Exception {
		int count = 0;
    	Connection conn = ds.getConnection();
    	String get = "select count(*) from masters where advisor=? and year=?";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	stmt.setInt(2,year);
    	ResultSet rs = stmt.executeQuery();
    	if(rs.next()){
    		count = rs.getInt(1);
    	}
    	
    	stmt.close();
   		conn.close();	
   		
    	return count;		
	}
	
	public double getPhdStudentForAdvisorCount(String advisor) throws Exception {
		double count = 0;
    	Connection conn = ds.getConnection();
    	String get = "select count(*) from phd where advisor=? and coadvisor=''";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	ResultSet rs = stmt.executeQuery();
    	if(rs.next()){
    		count = rs.getInt(1);
    	}
    	
    	get = "select count(*) from phd where coadvisor=? or (advisor=? and coadvisor!='')";
    	stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	stmt.setString(2,advisor);
    	rs = stmt.executeQuery();
    	int c = 0;
    	if(rs.next()){
    		c = rs.getInt(1);
    	}
    	
    	count += (c/2.0);
    	
    	stmt.close();
   		conn.close();	
   		
    	return count;		
	}
	
	public double getPhdStudentForAdvisorAndYearCount(String advisor,int year) throws Exception {
		double count = 0;
    	Connection conn = ds.getConnection();
    	String get = "select count(*) from phd where (advisor=? and coadvisor='') and year=?";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	stmt.setInt(2,year);
    	ResultSet rs = stmt.executeQuery();
    	if(rs.next()){
    		count = rs.getInt(1);
    	}
    	
    	get = "select count(*) from phd where coadvisor=? and year=?";
    	stmt = conn.prepareStatement(get);
    	stmt.setString(1,advisor);
    	stmt.setInt(2,year);
    	rs = stmt.executeQuery();
    	int c = 0;
    	if(rs.next()){
    		c = rs.getInt(1);
    	}
    	
    	count += (c/2.0);
    	
    	stmt.close();
   		conn.close();	
   		
    	return count;		
	}
	
	//Get all of the masters students
	public int getMastersStudentsCount() throws Exception {
		int count = 0;
    	Connection conn = ds.getConnection();
    	String get = "select count(*) from masters";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next())
    		count = rs.getInt(1);
    	
    	stmt.close();
   		conn.close();
		
    	return count;		
	}
	
	//Get all of the masters students
	public int getMastersStudentsForYearCount(int year) throws Exception {
		int count = 0;
    	Connection conn = ds.getConnection();
    	String get = "select count(*) from masters where year=?";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setInt(1,year);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next())
    		count = rs.getInt(1);
    	
    	stmt.close();
   		conn.close();
		
    	return count;		
	}
	
	//Get all phd students
	public int getPhdStudentsCount() throws Exception {
		int count = 0;
    	Connection conn = ds.getConnection();
    	String get = "select count(*) from phd";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next())
    		count = rs.getInt(1);
    	
    	stmt.close();
   		conn.close();
		
    	return count;		
	}
	
	//Get all phd students
	public int getPhdStudentsForYearCount(int year) throws Exception {
		int count = 0;
    	Connection conn = ds.getConnection();
    	String get = "select count(*) from phd where year=?";
    	PreparedStatement stmt = conn.prepareStatement(get);
    	stmt.setInt(1, year);
    	ResultSet rs = stmt.executeQuery();
    	while(rs.next())
    		count = rs.getInt(1);
    	
    	stmt.close();
   		conn.close();
		
    	return count;		
	}

}