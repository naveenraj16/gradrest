package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.MastersStudents;
import assign.domain.PhdStudents;
import assign.etl.DBQuery;



@Path("/")
public class TinmanResource {
	
	
	
	public TinmanResource(@Context ServletContext servletContext) {		
		
	}


@GET
@Path("/helloworld")
@Produces("text/html")
public String helloWorld() {
return "Hello world";
}

@GET
@Path("/ms")
@Produces("application/xml")
public Response getMastersStudent() throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final MastersStudents m = d.getMastersStudents();
	if(m != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputMastersStudents(outputStream, m);
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}

@GET
@Path("/ms/{year}")
@Produces("application/xml")
public Response getMastersStudentsForYear(@PathParam("year") int year) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final MastersStudents m = d.getMastersStudentsForYear(year);
	if(m != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputMastersStudents(outputStream, m);
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}

@GET
@Path("/phd")
@Produces("application/xml")
public Response getPhdStudents() throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final PhdStudents p = d.getPhdStudents();
	if(p != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputPhdStudents(outputStream, p);
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}

@GET
@Path("/phd/{year}")
@Produces("application/xml")
public Response getPhdStudentsForYear(@PathParam("year") int year) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final PhdStudents p = d.getPhdStudentsForYear(year);
	if(p != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputPhdStudents(outputStream, p);
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}

@GET
@Path("/{advisor}/ms")
@Produces("application/xml")
public Response getMastersStudentsForAdvisor(@PathParam("advisor") String advisor) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final MastersStudents m = d.getMastersStudentForAdvisor(advisor);
//System.out.println(m.getStudents());
	if(m != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputMastersStudents(outputStream, m);
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}

@GET
@Path("/{advisor}/ms/{year}")
@Produces("application/xml")
public Response getMastersStudentsForAdvisorAndYear(@PathParam("advisor") String advisor,@PathParam("year") int year) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final MastersStudents m = d.getMastersStudentForAdvisorAndYear(advisor,year);
//System.out.println(m.getStudents());
	if(m != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputMastersStudents(outputStream, m);
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}

@GET
@Path("/{advisor}/phd")
@Produces("application/xml")
public Response getPhdStudentsForAdvisor(@PathParam("advisor") String advisor) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final PhdStudents p = d.getPhdStudentsAdvisor(advisor);
	if(p != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputPhdStudents(outputStream, p);
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}

@GET
@Path("/{advisor}/phd/{year}")
@Produces("application/xml")
public Response getPhdStudentsForAdvisorAndYear(@PathParam("advisor") String advisor,@PathParam("year") int year) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final PhdStudents p = d.getPhdStudentsAdvisorAndYear(advisor,year);
	if(p != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputPhdStudents(outputStream, p);
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	else{
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
}


//COUNTS

@GET
@Path("/{advisor}/ms/count")
@Produces("application/xml")
public Response getMastersStudentsForAdvisorCounts(@PathParam("advisor") String advisor) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final int count = d.getMastersStudentForAdvisorCount(advisor);
	final String xmlString = "<MastersStudent><advisor>"+advisor+"</advisor><count>"+count+"</count></MastersStudent>";
	
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(xmlString);
				ps.close();
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	
	
}

@GET
@Path("/ms/count")
@Produces("application/xml")
public Response getMastersStudentsCounts() throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final int count = d.getMastersStudentsCount();
	final String xmlString = "<MastersStudent><count>"+count+"</count></MastersStudent>";
	
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(xmlString);
				ps.close();
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	
	
}

@GET
@Path("/ms/{year}/count")
@Produces("application/xml")
public Response getMastersStudentsForYearCounts(@PathParam("year") int year) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final int count = d.getMastersStudentsForYearCount(year);
	final String xmlString = "<MastersStudent><year>"+year+"</year><count>"+count+"</count></MastersStudent>";
	
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(xmlString);
				ps.close();
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	
	
	
}

@GET
@Path("/{advisor}/ms/{year}/count")
@Produces("application/xml")
public Response getMastersStudentsForAdvisorAndYearCount(@PathParam("advisor") String advisor,@PathParam("year") int year) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final int count = d.getMastersStudentForAdvisorAndYearCount(advisor,year);
	final String xmlString = "<MastersStudent><advisor>"+advisor+"</advisor><year>"+year+"</year><count>"+count+"</count></MastersStudent>";
	
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(xmlString);
				ps.close();
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	
	
}

@GET
@Path("/{advisor}/phd/count")
@Produces("application/xml")
public Response getPhdStudentsForAdvisorCount(@PathParam("advisor") String advisor) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final double count = d.getPhdStudentForAdvisorCount(advisor);
	final String xmlString = "<PhdStudent><advisor>"+advisor+"</advisor><count>"+count+"</count></PhdStudent>";
	
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(xmlString);
				ps.close();
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	
}

@GET
@Path("/phd/count")
@Produces("application/xml")
public Response getPhdStudentsForAdvisorCount() throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final int count = d.getPhdStudentsCount();
	final String xmlString = "<PhdStudent><count>"+count+"</count></PhdStudent>";
	
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(xmlString);
				ps.close();
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	
	
}

@GET
@Path("/phd/{year}/count")
@Produces("application/xml")
public Response getPhdStudentsForYearCount(@PathParam("year") int year) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final double count = d.getPhdStudentsForYearCount(year);
	final String xmlString = "<PhdStudent><year>"+year+"</year><count>"+count+"</count></PhdStudent>";
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(xmlString);
				ps.close();
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	
	
}

@GET
@Path("/{advisor}/phd/{year}/count")
@Produces("application/xml")
public Response getPhdStudentsForAdvisorAndYearCount(@PathParam("advisor") String advisor,@PathParam("year") int year) throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	final double count = d.getPhdStudentForAdvisorAndYearCount(advisor,year);
	final String xmlString = "<PhdStudent><advisor>"+advisor+"</advisor><year>"+year+"</year><count>"+count+"</count></PhdStudent>";
	
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(xmlString);
				ps.close();
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	
	
}

@GET
@Path("/advisors")
@Produces("application/xml")
public Response getAdvisorNames() throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	List<String> advisors = d.getAdvisorNames();
	String xmlString = "<advisors>";
	for(int i = 0; i < advisors.size(); i++){
		xmlString += "<advisor>"+advisors.get(i)+"</advisor>";
	}
	xmlString += "</advisors>";
	final String finalXmlString = xmlString;

	StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(finalXmlString);
				ps.close();
			}
	};
	Response response = Response.ok(so).build();
	response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
	return response;
}

@GET
@Path("/years")
@Produces("application/xml")
public Response getYears() throws Exception{
	DBQuery d = new DBQuery("jdbc:mysql://localhost:3306/grad","naveen","naveen123");
	List<Integer> years = d.getYears();
	String xmlString = "<years>";
	for(int i = 0; i < years.size(); i++){
		xmlString += "<year>"+years.get(i)+"</year>";
	}
	xmlString += "</years>";
	final String finalXmlString = xmlString;
	
	StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				final PrintStream ps = new PrintStream(outputStream);
				ps.print(finalXmlString);
				ps.close();
			}
	};
	Response response = Response.ok(so).build();
	response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
	return response;
	
}


protected void outputMastersStudents(OutputStream os, MastersStudents m) throws IOException {
	try {
		JAXBContext jaxbContext = JAXBContext.newInstance(MastersStudents.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(m, os);
} catch (JAXBException jaxb) {
	jaxb.printStackTrace();
	throw new WebApplicationException();
}
}

protected void outputPhdStudents(OutputStream os, PhdStudents p) throws IOException {
	try {
		JAXBContext jaxbContext = JAXBContext.newInstance(PhdStudents.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(p, os);
} catch (JAXBException jaxb) {
	jaxb.printStackTrace();
	throw new WebApplicationException();
}
}


}