package assign.resources;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class TinmanApplication extends Application {
	
private Set<Object> singletons = new HashSet<Object>();
private Set<Class<?>> classes = new HashSet<Class<?>>();

public TinmanApplication() {
}
@Override
public Set<Class<?>> getClasses() {
	classes.add(TinmanResource.class);
return classes;
}
@Override
public Set<Object> getSingletons() {
		return singletons;
}
}