package assign.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="student")
@XmlType(propOrder={"snum","sname","type","advisor","month","year"})
public class MastersStudent {
	
	private int snum;
	private String sname;
	private String type;
	private String advisor;
	private int month;
	private int year;
	
	public MastersStudent(int n,String s, String t, String a, int m, int y){
		snum = n;
		sname= s;
		type = t;
		advisor = a;
		month = m;
		year = y;
	}
	
	public MastersStudent(){
		
	}
	
	@XmlElement(name="snum")
	public int getSnum() {
		return snum;
	}
	
	public void setSnum(int snum) {
		this.snum = snum;
	}
	
	@XmlElement(name="sname")
	public String getSname() {
		return sname;
	}
	
	public void setSname(String sname) {
		this.sname = sname;
	}
	
	@XmlElement(name="type")
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	@XmlElement(name="advisor")
	public String getAdvisor() {
		return advisor;
	}
	
	public void setAdvisor(String advisor) {
		this.advisor = advisor;
	}
	
	@XmlElement(name="month")
	public int getMonth() {
		return month;
	}
	
	public void setMonth(int month) {
		this.month = month;
	}
	
	@XmlElement(name="year")
	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public String toString() {
		return sname;
	}
	
}
