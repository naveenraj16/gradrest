package assign.domain;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="phdStudent")
@XmlType(propOrder={"snum","sname","advisor","coadvisor","month","year"})
public class PhdStudent {

	private int snum;
	private String sname;
	private String advisor;
	private String coadvisor;
	private int month;
	private int year;
	
	public PhdStudent(int n,String s, String a, String ca, int m, int y){
		snum = n;
		sname= s;
		advisor = a;
		coadvisor = ca;
		month = m;
		year = y;
	}
	
	public PhdStudent(){
		
	}
	
	@XmlElement(name="snum")
	public int getSnum() {
		return snum;
	}
	
	public void setSnum(int snum) {
		this.snum = snum;
	}
	
	@XmlElement(name="sname")
	public String getSname() {
		return sname;
	}
	
	public void setSname(String sname) {
		this.sname = sname;
	}
	
	@XmlElement(name="advisor")
	public String getAdvisor() {
		return advisor;
	}
	public void setAdvisor(String advisor) {
		this.advisor = advisor;
	}
	
	@XmlElement(name="coadvisor")
	public String getCoadvisor() {
		return coadvisor;
	}
	
	public void setCoadvisor(String coadvisor) {
		this.coadvisor = coadvisor;
	}
	
	@XmlElement(name="month")
	public int getMonth() {
		return month;
	}
	
	public void setMonth(int month) {
		this.month = month;
	}
	
	@XmlElement(name="year")
	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
}
