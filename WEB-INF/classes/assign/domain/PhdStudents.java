package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "students")
public class PhdStudents {

	List<PhdStudent> students;

	public PhdStudents(List<PhdStudent> students) {
		super();
		this.students = students;
	}
	
	public PhdStudents() {
	}

	@XmlElement(name = "student")
	public List<PhdStudent> getStudents() {
		return students;
	}

	public void setStudents(List<PhdStudent> students) {
		this.students = students;
	}
	
}
