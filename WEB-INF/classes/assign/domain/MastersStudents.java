package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "students")
public class MastersStudents {

	List<MastersStudent> students;

	public MastersStudents(List<MastersStudent> students) {
		super();
		this.students = students;
	}
	
	public MastersStudents() {
	}

	@XmlElement(name = "student")
	public List<MastersStudent> getStudents() {
		return students;
	}

	public void setStudents(List<MastersStudent> students) {
		this.students = students;
	}
	
	
}
